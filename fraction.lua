local fraction = {
	_VERSION     = 'fraction v0.1.0',
}
local class = require('middleclass')

Fraction = class("fraction.Fraction")

local function gcd(a, b)
	if 0 == a or 0 == b then
		return 1
	end
	while b~=0 do 
		a, b = b, a % b
	end
	return math.abs(a)
end

local function lcm(a, b)
	return a / gcd(a, b) * b
end

function Fraction.static:reduce_num(a, b)
	local gcdn = gcd(a, b)
	return a / gcdn, b / gcdn
end

function Fraction.static:num_len(n)
	local len = 0
	n = math.abs(n)
	repeat
		n = math.floor(n / 10)
		len = len + 1
	until n == 0
	return len
end

local function to_frac(num)
    local W = math.floor(num)
    local F = num - W
    if F == 0 then
        return num, 1, 0
    end
    local pn, n, N = 0, 1
    local pd, d, D = 1, 0
    local x, err, q, Q
    repeat
        x = x and 1 / (x - q) or F
        q, Q = math.floor(x), math.floor(x + 0.5)
        pn, n, N = n, q*n + pn, Q*n + pn
        pd, d, D = d, q*d + pd, Q*d + pd
        err = F - N/D
    until math.abs(err) < 1e-15
    return N + D*W, D, err
end

local function isdecimal(num)
	return num ~= math.floor(num) and true or false
end

function Fraction:initialize(...)
	local up = select(1,...) or 0
	local down = select(2,...) or 1
	assert(down ~= 0, "Zero in denominators.")
	if isdecimal(up) or isdecimal(down) then
		self.up, self.down = to_frac(up / down)
		return
	else
		self.up, self.down = up, down
	end
	--self.up = up == nil and 0 or up
	--self.down = down == nil and 1 or down
	if down ~= 0 then
		self:reduce()
	end
end

function Fraction:reduce()
	self:fixsign()
	local gcdn = self:gcd()
	self.up = self.up / gcdn
	self.down = self.down / gcdn
end

function Fraction:fixsign()
	assert(self.down ~= 0, "Zero in denominators.")
	if 0 == self.up then
		self.down = 1
		return
	end
	if self.down < 0 then
		self.up = - self.up
		self.down = - self.down
		return
	end
end

function Fraction:gcd()
	return gcd(self.up, self.down)
end

function Fraction:lcm()
	return lcm(self.up, self.down)
end


function Fraction:__eq(b)
---[[
	assert(self.down ~= 0 and b.down ~= 0, "Zero in denominators.")
	if ((self.up == b.up and self.down == b.down) or (self.up == 0 and b.up == 0)) then
		return true
	else
		return false
	end
--]]
end

function Fraction:__lt(b)
	if (self.up < 0 and b.up > 0) then
		return true
	end
	local aup, bup = Fraction:reduce_num(self.up, b.up)
	local adown, bdown = Fraction:reduce_num(self.down, b.down)
	local resup = aup * bdown
	local resdown = adown * bup
	if resup < resdown then
		return true
	end
	return false
end

function Fraction:__unm(b)
	return Fraction:new(-b.up, b.down)
end

function Fraction:__add(n)
	local b
	if type(n) == "number" then
		b = Fraction:new(n)
	else
		b = n
	end
	local lcmn = lcm(self.down, b.down)
	local res = Fraction:new()
	res.up = lcmn / self.down * self.up + lcmn / b.down * b.up
	res.down = lcmn
	res:reduce()
	return res
end

function Fraction:__sub(b)
	return self + Fraction:new(-b.up, b.down)
end
---[[
function Fraction:__mul(b)
	local a = Fraction:new(self.up, self.down)
	local gcdn1 = gcd(a.up, b.down)
	local gcdn2 = gcd(a.down, b.up)
	a.up = a.up / gcdn1
	b.down = b.down / gcdn1
	a.down = a.down / gcdn2
	b.up = b.up / gcdn2
	a.up = a.up * b.up
	a.down = a.down * b.down
	a:fixsign()
	return a
end
--]]

function Fraction:__div(b)
	return self * Fraction:new(b.down, b.up)
end

function Fraction:pow(expt)
	local up = math.pow(self.up, expt)
	local down = math.pow(self.down, expt)
	return Fraction:new(up) / Fraction:new(down)
end

function Fraction:__tostring()
	if self.down == 1 then
		return self.up
	else
		return self.up .. '/' .. self.down
	end
end

return fraction