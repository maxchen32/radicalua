local radical = {
	_VERSION     = 'radical v0.1.0',
}
local class = require('middleclass')

Radical = class("radical.Radical")

function Radical:initialize(...)
	local up , down, radicand = 0, 1, 1
	local paralen = select('#',...)
	if paralen >= 3 then
		up = select(1,...) or 0
		down = select(2,...) or 1
		radicand = select(3,...) or 1
	elseif paralen == 1 then
		up = 1
		radicand = select(1,...) or 1
	elseif paralen == 2 then
		up = select(1,...) or 0
		radicand = select(2,...) or 1
	end
	assert(down ~= 0, "Zero in denominators.")
	local fractmp
	fractmp, self.radicand = Radical:radsqrt(radicand)
	self.out = fractmp * Fraction:new(up, down)
	if self.out.up == 0 then
		self.radicand = 1
	end
end

function Radical.static:radsqrt(radicand)
	assert(radicand >= 0, "Radsqrt: negative number, exiting.")
	local tmp = Fraction:new()
	local radi = 1
	if 0 == radicand then
		return tmp, radi
	end
	local flag = false
	for i = math.floor(math.sqrt(radicand)), 2, -1 do
		if 0 == radicand % (i * i) then
			tmp.up = i
			radi = radicand / (i * i)
			flag = true
			break
		end
	end
	if not flag then
		tmp.up = 1
		radi = radicand
	end
	return tmp, radi
end

function Radical:__tostring()
	local res = ""
	if self.out.up < 0 then res = res .. "-" end
	if self.radicand == 1 then
		if self.out.down == 1 then
			res = res .. math.abs(self.out.up)
		else
			res = res .. string.format("%d/%d", math.abs(self.out.up), self.out.down)
		end
	else
		if self.out.down == 1 then
			if self.out.up == 1 or self.out.up == -1 then
				res = res .. string.format("√(%d)", self.radicand)
			else
				res = res .. string.format("(%d)√(%d)", math.abs(self.out.up), self.radicand)
			end
		else
			res = res .. string.format("(%d/%d)√(%d)", math.abs(self.out.up), self.out.down, self.radicand)
		end
	end
	return res
end

function Radical.static:sqrtFrac(f)
	local u, d = Radical:new(f.up), Radical:new(f.down)
	local tmp = u.out / d.out
	u.radicand = u.radicand * d.radicand
	d.out.down = d.radicand
	d.out.up = 1
	u.out = tmp * d.out
	return u
end
